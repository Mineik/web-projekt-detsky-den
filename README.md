# Dětský den web
Hostováno na [bradaden.hudebniciulicnici.eu](bradaden.hudebniciulicnici.eu) <br/>
Wireframe a grafický návrh: [https://www.figma.com/file/YrT0CoRx8LgSQ6YFgsd2U9/Children-s-day](https://www.figma.com/file/YrT0CoRx8LgSQ6YFgsd2U9/Children-s-day)
## Jak spustit
* Vývojový server
    1) `yarn` - Nainstalování závislostí (alternativně: `npm i`)
    2) `yarn develop` - Spustí konfiguraci pro vývojový server (`npm run develop`)
* Statická stránka (produkční web)
    1) `yarn`  - Instalace závislostí
    2) `yarn compileAssets` - Konvertuje fotky z jpeg/jpg/png na webp
    3) `yarn export` - Vygeneruje statickou stránku
    4) Stránka se automaticky deployne na [Netlify](https://netlify.com) za každý push do `master` branche

## Technologie
* Node.js
* [Vue.js](https://vuejs.org)
* [SCSS/SASS](https://sass-lang.com)
* [Nuxt.js](https://nuxtjs.org)
* [Typescript](https://typescriptlang.org)

## Styl kódování
* Každá sekce je samostatná Vue.js komponenta
* Každá komponenta je uložena v [src/components](src/components), jednotlivé seke v [src/components/sections](src/components/sections)
* Pojmenování komponent se řídí [Konvencí pro pojmenovávání komponent ve Vue.js](https://vuejs.org/v2/guide/components-registration.html#Name-Casing)
* Styl je hlídaný ESLintem