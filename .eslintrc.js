module.exports = {
	root: true,
	parser: "@typescript-eslint/parser",
	plugins: [
		"@typescript-eslint",
	],
	extends: [
		"eslint:recommended",
		"plugin:@typescript-eslint/eslint-recommended",
		"plugin:@typescript-eslint/recommended",
	],
	rules: {
		indent: ["error", "tab"],
		"@typescript-eslint/ban-ts-comment": 0,
		"@typescript-eslint/indent":[2,"tab"]
	},

}
