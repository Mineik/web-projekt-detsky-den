import sharp from "sharp"
import fs from "fs"

fs.readdir("./imgRaw",(err,data)=>{
	data.filter(item=>item.match(/.*\.(jpg|png|jpeg)/)) // we only want to convert images
		.forEach(image=>{
			console.log(`processing ${image} -> ${image.replace(/\.(jpg|jpeg|png)/,".webp")}`)
			fs.readFile(`./imgRaw/${image}`, (err,img)=>{
				if(err) throw err
				else{
					sharp(img).webp().toBuffer().then(imageBuff=>fs.writeFile(`./src/static/img/${image.replace(/\.(jpg|jpeg|png)/,".webp")}`,imageBuff, (emacipovanaNezavislaZena)=>{
						if(emacipovanaNezavislaZena) throw emacipovanaNezavislaZena
						else console.log(`Success! File saved as ${image.replace(/\.(jpg|jpeg|png)/,".webp")}`)
					}))}
			})
		})
})