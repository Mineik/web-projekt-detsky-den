// nuxt.config.js
export default {
	buildModules: ['@nuxt/typescript-build'],
	modules:["@nuxtjs/style-resources"],
	srcDir: "./src",
	styleResources: {
		scss: ["~/styles/colors.scss","~/styles/mixins.scss"]
	},
	head:{
		meta:[
			{charset: 'utf-8'},
			{name: 'viewport', href: 'width=device-width, initial-scale=1'},
		],
		title: "Den dětí v Borském parku"
	},
	ssr: false
}
  
